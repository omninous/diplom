﻿using System.Linq;
using System.Web.Mvc;
using WebApplication1.Models;
using System.Data.Entity;
using System.Web;
using System.IO;
using System;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Ajax.Utilities;

namespace WebApplication1.Controllers
{
    public class AuthorController : Controller
    {
        private MaterialContext db = new MaterialContext();



        // Личный кабинет автора
        public ActionResult Index()
        {
            return View();
        }



        // Отправленные в редакцию материалы
        public ActionResult SentMaterials()
        {
            return View(db.Materials.ToList());
        }



        // Персональные данные автора
        public ActionResult PersonalDataAuthor()
        {
            return View();
        }



        // Детали публикации
        public ActionResult Details(int id = 0)
        {
            Material material = db.Materials.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }



        // Редактирование публикации
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Material material = db.Materials.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }
        [HttpPost]
        public ActionResult Edit(Material material)
        {
            db.Entry(material).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        // Добавление публикации в БД
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(HttpPostedFileBase upload, Material material)
        {
            // Отправка файла на сервер
            if (upload != null)
            {
                // Получение полного имени файла
                string fileName = Path.GetFileName(upload.FileName);
                // Сохранение полного имени файла в БД
                material.FileName = fileName;
                // Сохранение даты загрузки файла в БД
                material.DatePublication = DateTime.Now;



                var newAuthors = material.Authors.ToList();
                material.Authors.Clear();

                // Перебор всех авторов при создании
                foreach (var author in newAuthors)
                {
                    //Поиск в БД строки с таким же именем что и при создании
                    var authorBD = db.Authors.Where(m => m.Surname == author.Surname).Intersect(db.Authors.Where(m => m.Name == author.Name)).Intersect(db.Authors.Where(m => m.Patronymic == author.Patronymic)).Intersect(db.Authors.Where(m=>m.Place == author.Place)).ToList();

                    //Если созданная запись есть в БД
                    if (authorBD.Count != 0)
                    {
                        material.Authors.Add(authorBD[0]);
                    }
                    else
                    {
                        material.Authors.Add(author);
                    }
                    authorBD = null;
                }





                // Добавление данных в БД
                db.Materials.Add(material);
                db.SaveChanges();
                // Получение идентификатора публикации
                string identificator = Convert.ToString(material.Id);
                // Создание директории с папкой id
                Directory.CreateDirectory(Server.MapPath("~/Files/") + identificator);
                // сохранение файла на сервере
                upload.SaveAs(Server.MapPath("~/Files/" + identificator + "/" + fileName));
            }
            return RedirectToAction("Index");
        }



        // Удаление данных из БД
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Material material = db.Materials.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            return View(material);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Material material = db.Materials.Find(id);
            if (material == null)
            {
                return HttpNotFound();
            }
            db.Materials.Remove(material);
            db.SaveChanges();
            return RedirectToAction("Index");
        }






    }
}