﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        MaterialContext db = new MaterialContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Author()
        {
            return View();
        }

        public ActionResult Year2017()
        {
            return View();
        }

        public ActionResult Year2018()
        {
            return View();
        }

        public ActionResult Year2019()
        {
            return View();
        }

        public ActionResult Year2020()
        {
            return View();
        }

        public ActionResult Editorial()
        {
            return View();
        }

        public ActionResult Actual()
        {
            return View();
        }




        public ActionResult Y2017_1()
        {
            return View();
        }
        public ActionResult Y2017_2()
        {
            return View();
        }
        public ActionResult Y2017_3()
        {
            return View();
        }
        public ActionResult Y2017_4()
        {
            return View();
        }


        public ActionResult Y2018_1()
        {
            return View();
        }
        public ActionResult Y2018_2()
        {
            return View();
        }
        public ActionResult Y2018_3()
        {
            return View();
        }
        public ActionResult Y2018_4()
        {
            return View();
        }

        public ActionResult Y2019_1()
        {
            return View();
        }
        public ActionResult Y2019_2()
        {
            return View();
        }
        public ActionResult Y2019_3()
        {
            return View();
        }
        public ActionResult Y2019_4()
        {
            return View();
        }

        public ActionResult Y2020_1()
        {
            return View();
        }

    }
}