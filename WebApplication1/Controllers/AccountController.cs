﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        MaterialContext db = new MaterialContext();

        

        public ActionResult Index()
        {
            return View();
        }




        // Удаление данных из БД
        [HttpGet]
        public ActionResult Delete(int id)
        {
            Material b = db.Materials.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            return View(b);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Material b = db.Materials.Find(id);
            if (b == null)
            {
                return HttpNotFound();
            }
            db.Materials.Remove(b);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        // Получение файла с сервера
        public FileResult GetFile(int id)
        {
            Material material = db.Materials.Find(id);
            string file_name = material.FileName;
            // Путь к файлу
            string file_path = Server.MapPath("~/Files/" + Convert.ToString(id) + "/" + file_name);
            string file_type = "application/octet-stream";

            return File(file_path, file_type, file_name);
        }

    }
}