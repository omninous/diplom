﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class MaterialContext : DbContext
    {
        public DbSet<Material> Materials { get; set; }
        public DbSet<Author> Authors { get; set; }
    }


    //public class MaterialDBInitializer : DropCreateDatabaseAlways<MaterialContext>
    //{
    //    protected override void Seed(MaterialContext db)
    //    {

    //        db.Materials.Add(new Material 
    //        { 
    //            TitleRus = "ФИЗИЧЕСКИЕ ОСНОВЫ РАБОТЫ СОВРЕМЕННЫХ СИСТЕМ ПОЖАРОТУШЕНИЯ",
    //            TitleEng = "PHYSICAL BASIS OF WORK OF MODERN FIRE FIGHTING SYSTEMS",
    //            Type = "Научная статья",
    //            Rubric = "Приборостроение, метрология и информационно-измерительные приборы и системы",
    //            Pages = 21,
    //            Surname = "Жвакин ",
    //            Name = "Михаил",
    //            Patronymic = "Васильевич",
    //            Place = "ФГАОУ ВО «Северный (Арктический) федеральный университет имени М.В. Ломоносова»",
    //            Position = "КандидатТН",
    //            Email = "20madlzsnrt83456217@yandex.ru",
    //            AnnotationRus = "В статье рассматриваются актуальные вопросы современных систем пожаротушения, " +
    //            "физические основы работы, их достоинства и недостатки. Приведена классификация установок пожаротушения: " +
    //            "по конструктивному исполнению, по виду огнетушащего вещества, по способу тушения, по способу пуска, " +
    //            "по инерционности, по продолжительности подачи средств тушения. Существует два типа автоматических систем " +
    //            "пожаротушения: традиционные и модульные. Традиционные автоматические системы пожаротушения имеют много " +
    //            "конструкционных решений. Модульные автоматические системы пожаротушения состоят из отдельных модулей, " +
    //            "которые можно легко соединить для расширения системы, они быстро устанавливаются. Так же мы рассмотрим " +
    //            "водяные установки автоматического пожаротушения (АП). Такие установки могут быть двух видов: спринклерными и " +
    //            "дренчерными. Спринклерные системы состоят из трубопровода, который предназначен для передачи и распыления " +
    //            "пены или воды, насосов для увеличения давления при подаче воды, водохранилищ, датчиков и детекторов " +
    //            "звукового и светового оповещения. Дренчерные системы состоят из сети трубопроводов и расположенных по всей " +
    //            "площади оросителей, из которых для тушения пожаров поступает вода или пена. Пенные установки автоматического " +
    //            "пожаротушения, которые состоят из резервуара наполненного раствором пенообразователя, гидравлического " +
    //            "трубопровода, рукавов, генераторами пены и дозаторами. Газовые установки автоматического пожаротушения, " +
    //            "состоящие из нескольких модулей которые содержат огнетушащее вещество, трубные разводки, насадки, баллоны " +
    //            "с газом и пусковые устройства. Порошковые установки автоматического пожаротушения, которые состоят из " +
    //            "нескольких модулей: с централизованным источником рабочего газа и автономным источником рабочего газа. " +
    //            "Аэрозольные установки автоматического пожаротушения, состоящие из металлического корпуса, который выдерживает " +
    //            "внутреннее давление, аэрозолеобразующего наполнителя, пускового заряда, который размещён в узле запуска, " +
    //            "теплозащитной прокладки, охлаждающего модуля, отверстия из которого выходит аэрозоль, кронштейнов для крепления. " +
    //            "Рассмотрены группы и классы пожаров. Огонь может наносить серьезный ущерб, выходя из-под контроля человека, " +
    //            "образуя пожар.",
    //            AnnotationEng = "The article discusses current issues of modern fire extinguishing systems, the physical " +
    //            "basis of work, their advantages and disadvantages. The classification of fire extinguishing installations is given: " +
    //            "by design, by type of extinguishing agent, by method of extinguishing, by way of start-up, by inertia, by the " +
    //            "duration of supply of extinguishing agents. There are two types of automatic fire extinguishing systems: " +
    //            "traditional and modular. Traditional automatic fire extinguishing systems have many structural solutions. " +
    //            "Modular automatic fire extinguishing systems consist of separate modules that can be easily connected to " +
    //            "expand the system, they are quickly installed. We will also consider water systems for automatic fire " +
    //            "extinguishing (AP). Such installations can be of two types: sprinkler and deluge. Sprinkler systems consist " +
    //            "of a pipeline, which is designed to transfer and spray foam or water, pumps to increase the pressure when " +
    //            "water is supplied, reservoirs, sensors and detectors of sound and light alerts. Drainage systems consist of" +
    //            " a network of pipelines and sprinklers located throughout the area, from which water or foam enters to " +
    //            "extinguish fires. Foam automatic fire extinguishing installations, which consist of a reservoir filled " +
    //            "with a foaming agent solution, a hydraulic pipeline, hoses, foam generators and dispensers. Gas automatic " +
    //            "fire extinguishing installations, consisting of several modules that contain a fire extinguishing agent, piping, " +
    //            "nozzles, gas cylinders and starting devices. Powder-type automatic fire extinguishing installations, which" +
    //            " consist of several modules: with a centralized source of working gas and an autonomous source of working gas." +
    //            " Aerosol automatic fire extinguishing installations, consisting of a metal body that withstands internal " +
    //            "pressure, an aerosol-forming filler, a starting charge, which is placed in the start-up unit, a heat-shielding " +
    //            "gasket, a cooling module, the aerosol exit from its opening, brackets for fastening. Groups and classes of fires " +
    //            "are considered. Fire can cause serious damage, getting out of control of a person, forming a fire.",
    //            KeywordsRus = "давление, концентрация газа, группы пожаров, класс пожара, система автоматического пожаротушения, " +
    //            "огнетушащее вещество, традиционная и модульная установка, токсичное соединение",
    //            KeywordsEng = "pressure, gas concentration, fire groups, fire class, automatic fire extinguishing system, " +
    //            "extinguishing, agent, traditional and modular installation, toxic compound"
    //        });


    //        db.Materials.Add(new Material
    //        {
    //            TitleRus = "Подача произвольного сигнала на вход simulink - модели",
    //            TitleEng = "Non-standard signal input to simulink mode",
    //            Type = "Научная статья",
    //            Rubric = "Приборостроение, метрология и информационно-измерительные приборы и системы",
    //            Pages = 21,
    //            Surname = "Бильфельд",
    //            Name = "Николай",
    //            Patronymic = "Валентинович",
    //            Place = "ФГАОУ ВО «Северный (Арктический) федеральный университет имени М.В. Ломоносова»",
    //            Position = "КандидатТН",
    //            Email = "217@yandex.ru",
    //            AnnotationRus = "В статье рассматриваются актуальные вопросы современных систем пожаротушения, " +
    //            "физические основы работы, их достоинства и недостатки. Приведена классификация установок пожаротушения: " +
    //            "по конструктивному исполнению, по виду огнетушащего вещества, по способу тушения, по способу пуска, " +
    //            "по инерционности, по продолжительности подачи средств тушения. Существует два типа автоматических систем " +
    //            "пожаротушения: традиционные и модульные. Традиционные автоматические системы пожаротушения имеют много " +
    //            "конструкционных решений. Модульные автоматические системы пожаротушения состоят из отдельных модулей",
    //            AnnotationEng = "The article discusses current issues of modern fire extinguishing systems, the physical " +
    //            "basis of work, their advantages and disadvantages. The classification of fire extinguishing installations is given: " +
    //            "by design, by type of extinguishing agent, by method of extinguishing, by way of start-up, by inertia, by the " +
    //            "duration of supply of extinguishing agents. There are two types of automatic fire extinguishing systems",
    //            KeywordsRus = "давление, концентрация газа, группы пожаров, класс пожара, система автоматического пожаротушения, " +
    //            "огнетушащее вещество, традиционная и модульная установка, токсичное соединение",
    //            KeywordsEng = "pressure, gas concentration, fire groups, fire class, automatic fire extinguishing system, " +
    //            "extinguishing, agent, traditional and modular installation, toxic compound"
    //        });


    //        db.Materials.Add(new Material
    //        {
    //            TitleRus = "Автоматизированная система виртуального прохождения «Эскейп Рум» для студии квестов «Узник»",
    //            TitleEng = "Automated system of virtual passage “Escape room” for the studio quest “Prisoner” ",
    //            Type = "Научная статья",
    //            Rubric = "Приборостроение, метрология и информационно-измерительные приборы и системы",
    //            Pages = 21,
    //            Surname = "Мотова",
    //            Name = "Алена",
    //            Patronymic = "Сергеевна",
    //            Place = "ФГАОУ ВО «Северный (Арктический) федеральный университет имени М.В. Ломоносова»",
    //            Position = "КандидатТН",
    //            Email = "ffdssdd217@yandex.ru",
    //            AnnotationRus = "В статье рассматриваются актуальные вопросы современных систем пожаротушения, " +
    //            "физические основы работы, их достоинства и недостатки. Приведена классификация установок пожаротушения: " +
    //            "по конструктивному исполнению, по виду огнетушащего вещества, по способу тушения, по способу пуска",
    //            AnnotationEng = "The article discusses current issues of modern fire extinguishing systems, the physical " +
    //            "basis of work, their advantages and disadvantages. The classification of fire extinguishing installations is given: " +
    //            "by design, by type of extinguishing agent, by method of extinguishing, by way of start-up, by inertia, by the " +
    //            "duration of supply of extinguishing agents. There are two types of automatic fire extinguishing systems",
    //            KeywordsRus = "давление, концентрация газа, группы пожаров, класс пожара, система автоматического пожаротушения, " +
    //            "огнетушащее вещество, традиционная и модульная установка, токсичное соединение",
    //            KeywordsEng = "pressure, gas concentration, fire groups, fire class, automatic fire extinguishing system, " +
    //            "extinguishing, agent, traditional and modular installation, toxic compound"
    //        });

    //        base.Seed(db);
    //    }
    //}



}